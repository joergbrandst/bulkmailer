package com.brandstaett.bulkmailer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class BulkMailerApplication {
	
	static private final Logger logger = LoggerFactory.getLogger(BulkMailerApplication.class);

	public static void main(String[] args) throws Exception {
		ApplicationContext ctx = SpringApplication.run(BulkMailerApplication.class, args);
		Runner runner = ctx.getBean(Runner.class);
		runner.run(false);
		logger.info("Runner is finished");	
	}
}
