package com.brandstaett.bulkmailer;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Component
public class ExcelLoader {

	static private final Logger logger = LoggerFactory.getLogger(ExcelLoader.class);

	private DataFormatter dataFormatter = new DataFormatter();

	public List<Map<String,String>> load(String filename) throws IOException, InvalidFormatException {

		//ExcelReader reader = new ExcelReader(new File("adresses.xlsx"));
		Workbook wb = new XSSFWorkbook(new File(filename));
		FormulaEvaluator formulaEvaluator = wb.getCreationHelper().createFormulaEvaluator();

		Sheet sheet = wb.getSheetAt(0);
		Iterator<Row> rowIt = sheet.iterator();
		Row headerRow = rowIt.next();
		List<String> headers = new ArrayList<>();

		for(Iterator<Cell> cellIt = headerRow.cellIterator(); cellIt.hasNext();) {
			headers.add(cellIt.next().getStringCellValue());
		}

		List<Map<String,String>> data = new ArrayList<Map<String,String>>();
		while(rowIt.hasNext()) {
			Row row = rowIt.next();
			Map<String,String> rowData = new HashMap<String,String>();
			for(int i = 0; i< headers.size(); i++) {
				rowData.put(headers.get(i), dataFormatter.formatCellValue(row.getCell(i), formulaEvaluator));

			}
			data.add(rowData);
		}
		wb.close();
		return data;
	}
}
