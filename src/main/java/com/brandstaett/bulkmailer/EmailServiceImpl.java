package com.brandstaett.bulkmailer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.FileTemplateResolver;


@Service
public class EmailServiceImpl implements EmailService {

	private final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);
	private JavaMailSender mailSender;
	private File templatePath;
	private TemplateEngine templateEngine;
	private String mailFrom;
	private String mailFromName;
	private String replyTo;
	private String replyToName;
	private String bcc;
	private boolean testOnly = true;

	public static final String UTF8 = StandardCharsets.UTF_8.name();

	@Autowired
	public EmailServiceImpl(
			@Value("${mail.from.address}") String mailFrom,
			@Value("${mail.from.name}") String mailFromName,
			@Value("${mail.replyTo.address}") String replyTo,
			@Value("${mail.replyTo.name}") String replyToName,
			@Value("${mail.bcc}") String bcc,
			@Value("${testOnly}") boolean testOnly,
			JavaMailSender mailSender ) {
		this.mailSender = mailSender;
		this.templatePath = new File(".");

		this.mailFrom = mailFrom;
		this.mailFromName = mailFromName;
		this.replyTo = replyTo;
		this.replyToName = replyToName;
		this.bcc = bcc;
		this.testOnly = testOnly;

		logger.debug("Initializing EmailService with template-path " + templatePath.getAbsolutePath());
		FileTemplateResolver templateResolver = new FileTemplateResolver();
		templateResolver.setPrefix(templatePath.getAbsolutePath() + "/");
		templateResolver.setTemplateMode("HTML5");
		templateResolver.setCharacterEncoding("UTF-8");

		templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);

		// Disable cache so the template files can be changed without reloading the application
		templateEngine.setCacheManager(null); 	
	}

	@Override
	public void send(String to, String templateName, Map<String,String> variables, List<String> filesToAttach, Path outputDirectory) throws MessagingException {

		logger.debug("Sending email to " + to);
		final Context ctx = new Context();
		Map<String,Object> variableMap = new HashMap<>();
		variableMap.putAll(variables);
		ctx.setVariables(variableMap);

		final MimeMessage mimeMessage = mailSender.createMimeMessage();
		final MimeMessageHelper message = new MimeMessageHelper(mimeMessage,true,UTF8);
		message.setTo(to);
		message.setBcc(bcc);
		try {
			message.setFrom(new InternetAddress(this.mailFrom, this.mailFromName,UTF8));
			message.setReplyTo(new InternetAddress(this.replyTo, this.replyToName,UTF8));
		} catch (UnsupportedEncodingException e) { 
			throw new RuntimeException(e);
		}

		final String htmlContent = templateEngine.process(templateName, ctx);
		message.setText(htmlContent,true);
		// Find all element references starting with cid: and add the files to the mail
		Document doc = Jsoup.parse(htmlContent);
		Elements inlineImages = doc.select("img[src^=cid:]");
		for(Element element: inlineImages) {
			String src = element.attr("src");
			// It's assured that this array element is there because we filtered for cid:
			String filename = src.split("cid:")[1]; 
			File inlineFile =  new File(templatePath,filename);
			int extindex = filename.lastIndexOf(".");
			String extension = filename.substring(extindex + 1);
			String contentType = "image/" + extension;		
			logger.debug("Adding file " + inlineFile.getAbsolutePath() + " with contentType " + contentType);
			InputStreamSource fileSource = new FileSystemResource(inlineFile);
			message.addInline(filename, fileSource,contentType);		
		}



		String subject = doc.title();
		message.setSubject(subject);



		for(String pathtofile: filesToAttach) {
			File file = new File(pathtofile);
			if(file.canRead()) {
				String filename = file.getName();
				message.addAttachment(filename, file);
				logger.info("Attaching {}", filename);
			} else {
				logger.warn("Cannot read {}, not attaching file", pathtofile);
			}
		}

		String filename = to.replaceAll("@","_at_") + ".eml";
		File file = new File("out", filename);

		if(!testOnly) {
			mailSender.send(mimeMessage);
			mimeMessage.setSentDate(new Date());
		} else {
			logger.warn("testOnly is active, not sending");
		}

		try {
			Path existingDirectory;
			if(!Files.exists(outputDirectory)) {
				existingDirectory = Files.createDirectory(outputDirectory);
			} else {
				existingDirectory = outputDirectory;
			}
			Path pathToFile = existingDirectory.resolve(filename);
			logger.info("Writing to {}", pathToFile.toAbsolutePath().toString());
			OutputStream out = Files.newOutputStream(pathToFile);
			mimeMessage.writeTo(out);
			out.close();

		} catch (IOException e) {
			logger.error("Writing to {} failed", filename ,e);
		}

	}

}
