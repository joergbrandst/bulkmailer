package com.brandstaett.bulkmailer;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class Runner {

	static private final Logger logger = LoggerFactory.getLogger(Runner.class);

	@Value("${addressFile}")
	private String addressSource;

	@Value("${template}")
	private String template;

	@Value("${attachments}")
	private String[] attachments;

	@Value("${listAddresses}")
	private boolean listAddresses;

	@Autowired
	EmailService emailService;

	@Autowired
	private ExcelLoader excelLoader;

	public void run(boolean testOnly) throws Exception {



		String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm"));

		List<Map<String,String>> data = excelLoader.load(addressSource);
		if(listAddresses) {
			logger.warn("==== EMAILS WOULD GO TO =====");
		}

		for(Map<String,String> mailLine : data) {
			String to = mailLine.get("E-Mail");
			if(to != null && !to.trim().isEmpty()) {
				if(listAddresses) {
					logger.info(to);
				} else {
					logger.info("Email to {}", to);
					List<String> attachmentList = Arrays.asList(attachments);
					emailService.send(to, template, mailLine, attachmentList, Paths.get("EMAILS_" + timestamp));
				}
			}
		}
		if(listAddresses) {
			logger.warn("=========================");
		}

	}
}
