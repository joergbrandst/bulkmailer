package com.brandstaett.bulkmailer;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

public interface EmailService {

	/**
	 * Sends a email message
	 * @param to
	 * @param templateName the Thymeleaf-Template to generate the email from. The title-Element's content is used as subject
	 * @param variables Variables for the template
	 * @param
	 * @throws Exception if sending the email fails
	 */
	public void send(String to, String templateName, Map<String, String> variables, List<String> filesToAttach, Path outputDirectory) throws MessagingException;
	
}