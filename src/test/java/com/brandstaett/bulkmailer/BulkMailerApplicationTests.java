package com.brandstaett.bulkmailer;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest
public class BulkMailerApplicationTests {

	static private final Logger logger = LoggerFactory.getLogger(BulkMailerApplicationTests.class);

	@Autowired
	private EmailService emailService;

	@Value("${addressFile}")
	private String addressSource;

	@Value("${template}")
	private String template;

	//@Test
	public void contextLoads() {
		// Nothing else to do- fails if the context fails to load
	}

	//@Test
	public void sendMessage() throws MessagingException {
		logger.warn("Testing sendMessage");
		List<String> addresses = Arrays.asList("jb@resolution.de");
		List<String> attachments = Arrays.asList("satzungsaenderungsantrag.pdf","abc.pdf");
		addresses.forEach( address -> {
			try {
				emailService.send(address, template, Collections.emptyMap(), attachments, Paths.get("TESTOUT"));
			} catch (Exception e) {
				logger.error("Sending to {} failed", address);
			}
		});
	}
}
